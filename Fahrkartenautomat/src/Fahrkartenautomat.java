﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    double temp;
    
    while(true){
    	temp = fahrkartenbestellungErfassen();
    	temp = fahrkartenBezahlen(temp);
    	fahrkartenAusgeben();
    	rückgeldAusgabe(temp); 	 
    	}
    }
       
public static double fahrkartenbestellungErfassen(){
    double zuZahlenderBetrag = 1;
    final double einzelfahrschein = 2.90;
    final double tageskarte = 8.60;
    final double kleingruppenTageskarte = 23.50;
    int anzahlTickets;
    int tickettyp;
    boolean Valid = false;
    
    System.out.println("Fahrkartenbestellvorgang:\n"
    + "=========================\n\n"
    + "Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
    + "Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
    + "Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
    + "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
    Scanner tastatur = new Scanner(System.in);
    
    while(!Valid) {
    System.out.print("Ihre Wahl: ");
    tickettyp = tastatur.nextInt();
     
	if(tickettyp == 1){
	zuZahlenderBetrag = einzelfahrschein;
	Valid = true;
	}	
    else if(tickettyp == 2){
    zuZahlenderBetrag = tageskarte;
    Valid = true;
    }
    else if(tickettyp == 3){
    zuZahlenderBetrag = kleingruppenTageskarte;
    Valid = true;   
    }
    else{
    System.out.println(">>falsche Eingabe<<\n");   
    }
   }
    System.out.print("Anzahl der Tickets: ");
    anzahlTickets = tastatur.nextInt();
    if ((anzahlTickets < 1) || (anzahlTickets > 10)){
    	   anzahlTickets = 1;
    	   System.out.println("\nUngültige Ticketanzahl. \nGültige Anzahl 1-10 Tickets. \n1 Ticket wurde berechnet ");
       }
    zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;
       return zuZahlenderBetrag;
}

public static double fahrkartenBezahlen(double zuZahlenderBetrag){
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       Scanner tastatur = new Scanner(System.in);
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag){
    	   System.out.printf("Noch zu zahlen: %.2f EURO \n", ((double)Math.round(100 * (zuZahlenderBetrag - eingezahlterGesamtbetrag)) / 100));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }
           return (eingezahlterGesamtbetrag - zuZahlenderBetrag);
	}

public static void warte (int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }

public static void muenzeAusgeben(int betrag, String einheit) {
	System.out.println(betrag + " " + einheit);
	}

public static void fahrkartenAusgeben() {
       
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
        	  warte(250);
		}
       System.out.println("\n\n");
	}
public static void rückgeldAusgabe(double rückgabebetrag ) {
       
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO \n", ((double)Math.round(100 * rückgabebetrag) / 100));
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 1.999) // 2 EURO-Münzen
           {
        	  muenzeAusgeben(2, "EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 0.999) // 1 EURO-Münzen
           {
        	   muenzeAusgeben(1, "EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.4999) // 50 CENT-Münzen
           {
        	   muenzeAusgeben(50, "CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.1999) // 20 CENT-Münzen
           {
        	   muenzeAusgeben(20, "CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.0999) // 10 CENT-Münzen
           {
        	   muenzeAusgeben(10, "CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.04999)// 5 CENT-Münzen
           {
        	   muenzeAusgeben(5, "CENT");
 	          rückgabebetrag -= 0.05;
           }
       }
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}
    
