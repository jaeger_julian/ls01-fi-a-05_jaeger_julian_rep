package test;

public class Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.printf("0! %-5s = %-19s = %4s\n", "","","1");
		System.out.printf("1! %-5s = %-19s = %4s\n", "","1","1");
		System.out.printf("2! %-5s = %-19s = %4s\n", "","1 * 2","2");
		System.out.printf("3! %-5s = %-19s = %4s\n", "","1 * 2 * 3","6");
		System.out.printf("4! %-5s = %-19s = %4s\n", "","1 * 2 * 3 * 4","24");
		System.out.printf("5! %-5s = %-19s = %4s\n", "","1 * 2 * 3 * 4 * 5","120");
		
	}

}
