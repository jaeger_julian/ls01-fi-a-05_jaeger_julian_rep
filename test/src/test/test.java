package test;
public class test {
public static void main(String[] args) {
char ziffer = '1';
double grad, rad;
final double pi = 3.1415926535897932385;
int Zahl12 = 234567198;

System.out.println("ziffer + 25 = " + ziffer + 25);
System.out.println("ziffer + 25 = " + (ziffer + 25));
System.out.println("ziffer + 25 = " + (char)(ziffer + 25));
grad = 60.0;
rad = (grad / 180.0) * pi;
System.out.println("cos(" + grad + "°) = " + Math.cos(rad));
System.out.printf("%+020d%n", Zahl12); 
System.out.print("Hello Welt \n");
System.out.println("Hello Welt");
int alter = 56;
String name = "Willi";
System.out.printf("Ich heiße " + name + " und bin " + alter + " Jahre alt.\n");
System.out.printf("Hello %s!\n", "World");  
System.out.printf("Hello %s!", "World"); 
String s = "Java-Programm";
//Standardausgabe
System.out.printf( "\n|%s|\n", s );     // |Java-Programm|

//rechtsbündig mit 20 Stellen
System.out.printf( "|%20s|\n", s );     // |       Java-Programm|

//linksbündig mit 20 Stellen
System.out.printf( "|%-20s|\n", s );    // |Java-Programm       |

//minimal 5 stellen
System.out.printf( "|%5s|\n", s );      // |Java-Programm|

//maximal 4 Stellen
System.out.printf( "|%.4s|\n", s );     // |Java|

//20 Positionen, rechtsbündig, höchstens 4 Stellen von String
System.out.printf( "|%20.4s|\n", s );   // |                Java|

}
}